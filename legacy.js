// This is a helper to make it easier to use the API with code
// written with the older, more generic API

let warned = false
module.exports = function legacyProfile (ssb) {
  function matchCrut (type, recps) {
    switch (type) {
      case 'profile/person':
      case 'person':
        return recps
          ? ssb.profile.person.group
          : ssb.profile.person.public

      case 'profile/person/source':
      case 'person/source':
        return ssb.profile.person.source

      case 'profile/person/admin':
      case 'person/admin':
        return ssb.profile.person.admin

      case 'profile/community':
      case 'community':
        return recps
          ? ssb.profile.community.group
          : ssb.profile.community.public

      case 'profile/pataka':
      case 'pataka':
        if (!recps) return ssb.profile.pataka.public
    }
  }

  function loadCrut (profileId, cb) {
    if (!warned) {
      warned = true
      console.trace('ssb-profile/legacy used, please upgrade')
    }

    ssb.get({ id: profileId, private: true }, (err, value) => {
      if (err) return cb(err)

      if (typeof value.content === 'string') return cb(new Error('cannot decrypt this profile'))
      const crut = matchCrut(value.content.type, value.content.recps)

      if (crut) cb(null, crut)
      else cb(new Error(`No profile helpers for type ${value.content.type} with recps ${value.content.recps}`))
    })
  }

  return {
    create (type, details, cb) {
      if (!warned) {
        warned = true
        console.trace('ssb-profile/legacy used, please upgrade')
      }
      return matchCrut(type, details.recps).create(details, cb)
    },
    get (profileId, cb) {
      loadCrut(profileId, (err, crut) => {
        if (err) return cb(err)
        crut.get(profileId, cb)
      })
    },
    update (profileId, details, cb) {
      loadCrut(profileId, (err, crut) => {
        if (err) return cb(err)
        crut.update(profileId, details, cb)
      })
    },
    tombstone (profileId, details, cb) {
      loadCrut(profileId, (err, crut) => {
        if (err) return cb(err)
        crut.tombstone(profileId, details, cb)
      })
    }
  }
}
