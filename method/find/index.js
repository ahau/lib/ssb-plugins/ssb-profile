/* eslint-disable camelcase */
/* eslint-disable brace-style */
const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const { where, and, or, type: msgType, toPullStream } = require('ssb-db2/operators')
const { deferred } = require('jitdb/operators')

const NameIndex = require('./name-index')
const normalize = require('./normalize')

module.exports = function Find (ssb, getProfile) {
  ssb.db.registerIndex(NameIndex)
  ssb.db.operators.nameMentions = nameMentions

  function nameMentions (name) {
    return deferred((meta, cb) => {
      meta.db.onDrain('nameMentions', () => {
        meta.db.getIndex('nameMentions').getMessagesByNameMention(name, meta.live, cb)
      })
    })
  }

  function isProfileType (type) {
    return typeof type === 'string'
      ? msgType('profile/' + type)
      : or(
        msgType('profile/person'),
        msgType('profile/person/admin'),
        msgType('profile/person/source'),
        msgType('profile/community')
      )
  }

  return function find (opts, cb) {
    const {
      name,
      type = 'person', // NOTE can be null
      groupId,
      includeTombstoned = false,
      decorateWithLinkedFeeds = false
    } = opts

    if (typeof name !== 'string' || name.length < 3) return cb(new Error('opts.name must be a String of length >= 3'))
    buildRecpsFilter(groupId, (err, recpsFilter) => {
      if (err) return cb(err)

      pull(
        ssb.db.query(
          where(
            and(
              nameMentions(name),
              isProfileType(type)
            )
          ),
          toPullStream()
        ),

        // ? filter by recps[0]
        recpsFilter ? pull.filter(recpsFilter) : null,

        pull.map(getProfileId),
        pull.unique(),

        // load the profile record
        paraMap(
          (profileId, cb) => {
            getProfile(profileId, (err, profile) => cb(null, err ? null : profile)) // swallow errors
          },
          5 // width
        ),
        pull.filter(Boolean), // Filter Out errors (null)

        // ? filter tombstoned
        (!includeTombstoned) ? pull.filter(profile => profile.tombstone === null) : null,

        // filter by name
        pull.filter(buildNameFilter(name)),

        // ? add profile.linkedFeeds
        decorateWithLinkedFeeds ? paraMap(addLinkedFeeds, 5) : null,

        pull.collect(cb)
      )
    })
  }

  function buildRecpsFilter (groupId, cb) {
    if (!groupId) return cb(null, null)

    const isGroupEncrypted = m => m.value.content.recps?.[0] === groupId
    if (!groupId.endsWith('cloaked')) return cb(null, isGroupEncrypted)

    ssb.tribes.poBox.get(groupId, (err, poBox) => {
      if (err) return cb(null, isGroupEncrypted)

      const isPOBoxEncrypted = m => m.value.content.recps?.[0] === poBox.poBoxId
      return cb(null, m => isGroupEncrypted(m) || isPOBoxEncrypted(m))
    })
  }

  function buildNameFilter (name) {
    const regexp = new RegExp(normalize(name), 'i')

    const isMatch = (personName) => {
      if (!personName) return false
      return normalize(personName).match(regexp)
    }

    return function nameFilter (profile) {
      const { preferredName, legalName, altNames } = profile

      if (isMatch(preferredName)) return true
      if (isMatch(legalName)) return true
      if (altNames && altNames.find(isMatch)) return true

      return false
    }
  }

  function addLinkedFeeds (profile, cb) {
    profile.linkedFeeds = []

    ssb.profile.findFeedsByProfile(profile.key, (err, feeds) => {
      if (err) return cb(err)

      profile.linkedFeeds = Array.from(new Set(feeds))

      cb(null, profile)
    })
  }
}

function getProfileId (msg) {
  return msg.value.content.tangles.profile.root || msg.key
}
