// Copied from ssb-db2/indexes/full-mentions.js
/* eslint-disable camelcase, brace-style */

const bipf = require('bipf')
const pull = require('pull-stream')
const pl = require('pull-level')
const { or, seqs, liveSeqs } = require('ssb-db2/operators')

const Plugin = require('./plugin')
const normalize = require('./normalize')

const BIPF_KEY = bipf.allocAndEncode('key')
const BIPF_CONTENT = bipf.allocAndEncode('content')
const BIPF_PRF_NAME = bipf.allocAndEncode('preferredName')
const BIPF_LGL_NAME = bipf.allocAndEncode('legalName')
const BIPF_ALT_NAME = bipf.allocAndEncode('altNames')
const BIPF_SET = bipf.allocAndEncode('set')

// [destMsgId, origShortMsgId] => seq
module.exports = class NameIndex extends Plugin {
  constructor (log, dir, configDb2) {
    super(log, dir, 'nameMentions', 1, 'json', null, configDb2)
  }

  processRecord (record, seq, pValue) {
    const buf = record.value

    let p = 0 // note you pass in p!
    p = bipf.seekKey2(buf, pValue, BIPF_CONTENT, 0)
    if (p < 0) return

    let shortKey

    // check for preferredName
    let p_prf = bipf.seekKey2(buf, p, BIPF_PRF_NAME, 0)
    if (p_prf >= 0) {
      p_prf = bipf.seekKey2(buf, p_prf, BIPF_SET, 0)
      if (p_prf >= 0) {
        const preferredName = bipf.decode(buf, p_prf)
        if (preferredName) {
          shortKey = shortKey || getShortKey(buf)

          this.batch.push({
            type: 'put',
            key: [normalize(preferredName), shortKey],
            value: seq
          })

          const splitNames = preferredName.split(' ')
          if (splitNames.length > 1) {
            splitNames.forEach(name => {
              this.batch.push({
                type: 'put',
                key: [normalize(name), shortKey],
                value: seq
              })
            })
          }
        }
      }
    }

    // check for legalName
    let p_lgl = bipf.seekKey2(buf, p, BIPF_LGL_NAME, 0)
    if (p_lgl >= 0) {
      p_lgl = bipf.seekKey2(buf, p_lgl, BIPF_SET, 0)
      if (p_lgl >= 0) {
        const legalName = bipf.decode(buf, p_lgl)
        if (legalName) {
          shortKey = shortKey || getShortKey(buf)

          this.batch.push({
            type: 'put',
            key: [normalize(legalName), shortKey],
            value: seq
          })

          const splitNames = legalName.split(' ')
          if (splitNames.length > 1) {
            splitNames.forEach(name => {
              this.batch.push({
                type: 'put',
                key: [normalize(name), shortKey],
                value: seq
              })
            })
          }
        }
      }
    }

    // check for altNames
    const p_alt = bipf.seekKey2(buf, p, BIPF_ALT_NAME, 0)
    if (p_alt >= 0) {
      const altNames = bipf.decode(buf, p_alt)
      if (isObject(altNames)) {
        shortKey = shortKey || getShortKey(buf)
        Object.entries(altNames).forEach(([altName, score]) => {
          if (typeof score === 'number' && score > 0) {
            this.batch.push({
              type: 'put',
              key: [normalize(altName), shortKey],
              value: seq
            })

            const splitNames = altName.split(' ')
            if (splitNames.length > 1) {
              splitNames.forEach(name => {
                this.batch.push({
                  type: 'put',
                  key: [normalize(name), shortKey],
                  value: seq
                })
              })
            }
          }
        })
      }
    }
  }

  indexesContent () {
    return true
  }

  getMessagesByNameMention (name, live, cb) {
    name = normalize(name)
    // TODO allow non-exact matches on name
    const opts = {
      lte: [name + '~', undefined],
      gte: [name, null],
      keyEncoding: this.keyEncoding,
      keys: false
    }

    pull(
      pl.read(this.level, opts),
      pull.collect((err, seqArr) => {
        if (err) return cb(new Error('nameMentions.getMessagesByNameMention() failed to read leveldb', { cause: err }))

        if (live) {
          const ps = pull(
            pl.read(this.level, Object.assign({}, opts, { live, old: false })),
            pull.map(parseInt10)
          )
          cb(null, or(seqs(seqArr.map(parseInt10)), liveSeqs(ps)))
        }
        else cb(null, seqs(seqArr.map(parseInt10)))
      })
    )
  }
}

function parseInt10 (x) {
  return parseInt(x, 10)
}

function getShortKey (buf) {
  const pKey = bipf.seekKey2(buf, 0, BIPF_KEY, 0)
  return bipf.decode(buf, pKey).slice(1, 10)
}

function isObject (obj) {
  if (obj === null) return false
  if (Array.isArray(obj)) return false
  return typeof obj === 'object'
}
