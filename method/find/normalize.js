// source: https://stackoverflow.com/questions/990904/remove-accents-diacritics-in-a-string-in-javascript
module.exports = function normalize (name) {
  return name
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .toLowerCase()
}
