const { isMsgId } = require('ssb-ref')
const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const pileSort = require('pile-sort')
const { where, type, toPullStream } = require('ssb-db2/operators')

module.exports = function FindAdminProfileLinks (ssb, crut) {
  return function findAdminProfileLinks (profileId, opts = {}, cb) {
    if (typeof opts === 'function') return findAdminProfileLinks(profileId, {}, opts)
    if (!isMsgId(profileId)) return cb(new Error('requires a valid profileId'))

    pull(
      // TODO replace with crut.list
      ssb.db.query(
        where(type('link/profile-profile/admin')),
        toPullStream()
      ),
      pull.filter(crut.isRoot),
      pull.filter(m => (
        m.value.content.parent === profileId ||
        m.value.content.child === profileId
        // just quickly check to see if this backlink is a related with
        // our target profileId (as parent or child)
      )),
      pull.filter(Boolean), // drop null profiles
      paraMap((rawLink, cb) => {
        crut.read(rawLink.key, cb)
      }, 4),
      pull.filter(link => link.tombstone === null),
      pull.collect((err, results) => {
        if (err) return cb(err)

        const piles = pileSort(results, [
          link => link.child === profileId
        ])

        cb(null, { parentLinks: piles[0], childLinks: piles[1] })
      })
    )
  }
}
