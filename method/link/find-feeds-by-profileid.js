const { isMsgId } = require('ssb-ref')
const pull = require('pull-stream')
const { where, type, toPullStream } = require('ssb-db2/operators')

module.exports = function FindFeedsByProfile (ssb, crut) {
  return function findFeedsByProfile (profileId, opts = {}, cb) {
    if (typeof opts === 'function') return findFeedsByProfile(profileId, {}, opts)
    if (!isMsgId(profileId)) return cb(new Error('requires a valid profileId'))

    const {
      selfLinkOnly = true
    } = opts

    pull(
      ssb.db.query(
        where(type('link/feed-profile')),
        toPullStream()
      ),
      pull.filter(crut.isRoot),
      pull.filter(m => m.value.content.child === profileId),
      // TODO check if the links are tombstoned!

      selfLinkOnly
        ? pull.filter(link => link.value.author === link.value.content.parent)
        // you can only link yourself to a profile
        : null,

      pull.collect((err, links) => {
        if (err) return cb(err)

        const results = selfLinkOnly
          ? links.reduce(selfOnlyReduce, [])
          : links.reduce(selfOtherReduce, { self: [], other: [] })

        cb(null, results)
      })
    )
  }
}

function selfOnlyReduce (acc, link) {
  const feedId = link.value.content.parent

  if (!acc.includes(feedId)) acc.push(feedId)

  return acc
}
// []

function selfOtherReduce (acc, link) {
  const feedId = link.value.content.parent

  if (feedId === link.value.author) {
    if (!acc.self.includes(feedId)) acc.self.push(feedId)
  } // eslint-disable-line
  else {
    if (!acc.other.includes(feedId)) acc.other.push(feedId)
  }

  return acc
}
// { self: [], other: [] }
