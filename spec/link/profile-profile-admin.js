module.exports = {
  type: 'link/profile-profile/admin',
  staticProps: {
    parent: { $ref: '#/definitions/messageId', required: true },
    child: { $ref: '#/definitions/messageId', required: true }
  },
  props: {}
}
