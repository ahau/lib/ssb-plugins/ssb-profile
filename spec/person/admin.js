// An Admin profile of a person for kaitiaki only
const SimpleSet = require('@tangle/simple-set')

const { string, gender, number, boolean, image, stringArray, customFields, profileSource } = require('../lib/field-types')
const { isKaitiakiEncrypted, hasValidAliveInterval, hasValidDateCustomFields } = require('../lib/validators')
const GetTransformation = require('../lib/get-transformation')

const personAdminSpec = {
  type: 'profile/person/admin',
  props: {
    preferredName: string,
    legalName: string,
    altNames: SimpleSet(),

    avatarImage: image,
    headerImage: image,

    gender,
    source: profileSource,
    description: string,

    aliveInterval: string,
    deceased: boolean,
    placeOfBirth: string,
    placeOfDeath: string,
    buriedLocation: string,
    birthOrder: number,

    city: string,
    country: string,
    postCode: string,

    profession: string,
    education: stringArray,
    school: stringArray,

    /* non-group */
    phone: string,
    email: string,
    address: string,

    customFields
  },
  hooks: {
    isRoot: [isKaitiakiEncrypted, hasValidAliveInterval, hasValidDateCustomFields],
    isUpdate: [isKaitiakiEncrypted, hasValidAliveInterval, hasValidDateCustomFields]
  }
}
personAdminSpec.getTransformation = GetTransformation(personAdminSpec)
// needed to prune legacy attributes, e.g. location

module.exports = personAdminSpec
