// ssb blob
const makeSsbBlob = (mimeType) => ({ // blob for pdf file
  type: 'ssb',
  blobId: '&1ZQM7TjQHBUEcdBijB6y7dkX047wCf4aXcjFplTjrJo=.sha256',
  mimeType,
  size: 12345,
  unbox: 'YmNz1XfPw/xkjoN594ZfE/JUhpYiGyOOQwNDf6DN+54=.boxs'
})

const makeHyperBlob = (mimeType) => ({
  type: 'hyper',
  driveAddress: '6LJVf5C5T1WauuJUgCWUA0mT+ak9pV5fFH4uhw2PeVU=',
  blobId: '2df0cb34-77e5-40af-99ad-52d85ff67d35',
  readKey: 'Wenusia9d1gdhJNGsgY+66xRjgbl82N7daFeaM436qs=',
  mimeType,
  size: 12345
})

const blob1 = makeSsbBlob('application/pdf')
const blob2 = makeSsbBlob('image/jpeg')
const blob3 = makeHyperBlob('video/mp4')
const blob4 = makeHyperBlob('audio/mpeg')

module.exports = {
  blob1,
  blob2,
  blob3,
  blob4
}
