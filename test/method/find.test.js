const test = require('tape')
const pull = require('pull-stream')
const pullParaMap = require('pull-paramap')
const pick = require('lodash.pick')
const { promisify: p } = require('util')

const Server = require('../test-bot')

function setup (profileDetails, next) {
  const server = Server({ tribes: true })

  server.tribes.create({}, (err, { groupId } = {}) => {
    if (err) throw err

    pull(
      pull.values(profileDetails),
      pullParaMap(({ type, details, update, link }, cb) => {
        details.authors = { add: [server.id] }
        if (details.recps) details.recps = [groupId]
        const [superType, subType] = getTypePath(type, details.recps)

        const crut = server.profile[superType][subType]

        crut.create(details, (err, profileId) => {
          if (err) return cb(err)

          if (!update) {
            if (!link) return cb(null, profileId)
            server.profile.link.create(profileId, (err, link) => {
              if (err) return cb(err)

              cb(null, profileId)
            })
          } else crut.update(profileId, update, cb)
        })
      }, 4),
      pull.collect((err, profiles) => {
        if (err) throw err
        next(server, groupId)
      })
    )
  })

  function getTypePath (typeStr, recps) {
    const typePath = typeStr.split('/')
    if (typePath.length === 1) typePath.push(recps ? 'group' : 'public')

    return typePath
  }
}

test('find - search "ben" and get all the people with "ben*" in any name', t => {
  t.plan(5)
  const input = [
    {
      type: 'person',
      details: { preferredName: 'Ben1' }
    },
    {
      type: 'person',
      details: { preferredName: 'ben2' }
    },
    {
      type: 'person',
      details: { legalName: 'dave ben', recps: true }
    },
    {
      type: 'person',
      details: { altNames: { add: ['benjamin'] }, recps: true }
    },
    // not this one
    {
      type: 'person',
      details: { preferredName: 'mr bean' }
    }
  ]

  setup(
    input,
    (server) => {
      server.profile.find({ type: 'person', name: 'ben' }, (err, profiles) => {
        t.error(err, 'runs find')

        t.true(
          profiles.every(p => (
            (p.preferredName && p.preferredName.match(/ben/i)) ||
            (p.legalName && p.legalName.match(/ben/i)) ||
            (p.altNames && p.altNames.find(name => name.match(/ben/i)))
          )),
          'all results contain search term "ben"'
        )

        t.equal(profiles.length, 4, 'finds right number of profiles')

        server.profile.find({ type: 'person', name: 'bean' }, (err, profiles) => {
          t.error(err, 'runs find')

          t.equal(profiles.length, 1, 'finds one "bean"')
          server.close()
        })
      })
    }
  )
})

test('find - search "ben t" and get all the people with "ben t*" in their name', t => {
  t.plan(6)
  const input = [
    // should match these two
    {
      type: 'person',
      details: { legalName: 'Ben Tairea', recps: true }
    },
    {
      type: 'person',
      details: { preferredName: 'ben tairea' }
    },

    // but not these ones
    {
      type: 'person',
      details: { legalName: 'dave ben', recps: true }
    },
    {
      type: 'person',
      details: { altNames: { add: ['benjamin tairea'] }, recps: true }
    },
    {
      type: 'person',
      details: { preferredName: 'mr bean' }
    }
  ]

  setup(
    input,
    (server) => {
      server.profile.find({ type: 'person', name: 'ben t' }, (err, profiles) => {
        t.error(err, 'runs find')

        t.true(profiles.length !== 0, 'search returned results')

        t.true(
          profiles.every(p => (
            (p.preferredName && p.preferredName.match(/ben t/i)) ||
            (p.legalName && p.legalName.match(/ben t/i)) ||
            (p.altNames && p.altNames.find(name => name.match(/ben t/i)))
          )) && profiles.length !== 0,
          'all results contain search term "ben t"'
        )

        t.equal(profiles.length, 2, 'finds right number of profiles')

        server.profile.find({ type: 'person', name: 'bean' }, (err, profiles) => {
          t.error(err, 'runs find')

          t.equal(profiles.length, 1, 'finds one "bean"')

          server.close()
        })
      })
    }
  )
})

test('find - scoped to correct profile type', t => {
  t.plan(1)

  setup(
    [
      {
        type: 'person',
        details: { preferredName: 'aurora (person)' }
      },
      {
        type: 'community',
        details: { preferredName: 'aurora (community)' }
      }
    ],
    (server) => {
      server.profile.find({ type: 'community', name: 'aur' }, (err, profiles) => {
        if (err) throw err

        t.deepEqual(
          profiles.map(p => pick(p, ['type', 'preferredName'])),
          [{ type: 'community', preferredName: 'aurora (community)' }],
          'type: community - searches the right types'
        )

        server.close()
      })
    }
  )
})

test('find - type: null', t => {
  t.plan(1)

  setup(
    [
      {
        type: 'person',
        details: {
          preferredName: 'aurora (person)',
          recps: true
        }
      },
      {
        type: 'person/source',
        details: {
          preferredName: 'aurora (source)',
          recps: true
        }
      }
    ],
    (server, groupId) => {
      server.profile.find({ type: null, name: 'aur' }, (err, profiles) => {
        if (err) throw err
        profiles = profiles
          .map(profile => ({
            ...pick(profile, ['type', 'preferredName', 'recps']),
            recps: profile.recps
          }))
          .reverse()

        t.deepEqual(
          profiles.sort((a, b) => a.preferredName.localeCompare(b.preferredName)),
          [
            { type: 'person', preferredName: 'aurora (person)', recps: [groupId] },
            { type: 'person/source', preferredName: 'aurora (source)', recps: [groupId] }
          ].sort((a, b) => a.preferredName.localeCompare(b.preferredName)),
          'type: null - searches the right types'
        )

        server.close()
      })
    }
  )
})

test('find - scope by groupId', t => {
  t.plan(1)

  setup(
    [
      {
        type: 'person',
        details: {
          preferredName: 'aurora (public)'
        }
      },
      {
        type: 'person',
        details: {
          preferredName: 'aurora (group)',
          recps: true
        }
      }
    ],
    (server, groupId) => {
      server.profile.find({ type: 'person', groupId, name: 'aur' }, (err, profiles) => {
        if (err) throw err
        profiles = profiles
          .map(profile => ({
            ...pick(profile, ['type', 'preferredName', 'recps']),
            recps: profile.recps
          }))

        t.deepEqual(
          profiles,
          [
            { type: 'person', preferredName: 'aurora (group)', recps: [groupId] }
          ],
          'groupId - returns only methods profiles in right group'
        )

        server.close()
      })
    }
  )
})

test('find - tombstone records', t => {
  t.plan(2)

  setup(
    [
      {
        type: 'person',
        details: { preferredName: 'big ben' },
        update: { tombstone: { date: Date.now() } }
      }
    ],
    (server) => {
      server.profile.find({ type: 'person', name: 'ben' }, (_, profiles) => {
        t.equal(profiles.length, 0, 'does not find tombstoned records')

        server.profile.find({ type: 'person', name: 'ben', includeTombstoned: true }, (_, profiles) => {
          t.equal(profiles.length, 1, 'includes tombstoned records (includeTombstoned: true)')

          server.close()
        })
      })
    }
  )
})

test('find - updates records', t => {
  t.plan(1)

  setup(
    [
      {
        type: 'person',
        details: { preferredName: 'benjamin' },
        update: { preferredName: 'jim' }
      },
      {
        type: 'person',
        details: { preferredName: 'bj' },
        update: { preferredName: 'ben james' }
      }
    ],
    (server) => {
      server.profile.find({ type: 'person', name: 'Ben' }, (_, profiles) => {
        t.deepEqual(
          profiles.map(p => p.preferredName),
          ['ben james'],
          'takes updates into account'
        )

        server.close()
      })
    }
  )
})

test('find - test link decoration', t => {
  t.plan(1)

  setup(
    [
      {
        type: 'person',
        details: { preferredName: 'benjamin' },
        link: true
      },
      {
        type: 'person',
        details: { preferredName: 'ben' },
        link: false
      }
    ],
    (server) => {
      server.profile.find({ type: 'person', name: 'Ben', decorateWithLinkedFeeds: true }, (_, profiles) => {
        t.deepEqual(
          profiles.map(p => p.linkedFeeds).sort((a, b) => a.length - b.length),
          [[], [server.id]].sort((a, b) => a.length - b.length),
          'decorate with linked feeds'
        )

        server.close()
      })
    }
  )
})

test('find (admin profiles)', async t => {
  const ssb = Server({ tribes: true })

  const { groupId, poBoxId } = await p(ssb.tribes.create)({ addPOBox: true })

  const profileId1 = await p(ssb.profile.person.admin.create)({
    preferredName: 'dave one',
    authors: { add: ['*'] },
    recps: [groupId]
  })

  const profileId2 = await p(ssb.profile.person.admin.create)({
    preferredName: 'dave two',
    authors: { add: ['*'] },
    recps: [poBoxId]
  })

  const profiles = await p(ssb.profile.find)({ name: 'dave', type: 'person/admin', groupId })

  t.deepEqual(
    profiles.map(p => p.key).sort(),
    [profileId2, profileId1].sort(),
    'finds profiles from groupId and associated profileId'
  )

  ssb.close()
  t.end()
})

test('find (with macron)', t => {
  t.plan(4)
  /* search "maui" and get all the people with "maui" in in any name, including with macrons and special characters */
  setup(
    [
      {
        type: 'person',
        details: { preferredName: 'Māui-pōtiki' } // with macrons
      },
      {
        type: 'person',
        details: { preferredName: 'Maui' } // without macrons
      }
    ],
    (server) => {
      server.profile.find({ type: 'person', name: 'maui' }, (err, profilesA) => {
        t.error(err, 'runs find on maui (no macron)')

        profilesA = profilesA.map(p => p.preferredName)

        t.deepEqual(
          profilesA.sort(),
          ['Maui', 'Māui-pōtiki'].sort(),
          'returns both maui profiles'
        )

        server.profile.find({ type: 'person', name: 'māui' }, (err, profilesB) => {
          t.error(err, 'runs find on māui (macron)')
          profilesB = profilesB.map(p => p.preferredName)

          t.deepEqual(profilesA.sort(), profilesB.sort(), 'returns same for search without macrons')
          server.close()
        })
      })
    }
  )
})

test('find (empty)', t => {
  t.plan(2)

  setup(
    [],
    (server) => {
      server.profile.find({ type: 'person', name: '' }, (err) => {
        t.match(err.message, /opts.name must be a String/, 'throws error on empty string')

        server.profile.find({ type: 'person', name: null }, (err) => {
          t.match(err.message, /opts.name must be a String/, 'throws error on null string')

          server.close()
        })
      })
    }
  )
})

test('find (null profile fields)', t => {
  t.plan(3)

  setup(
    [
      {
        type: 'person',
        details: {
          preferredName: 'Cherese',
          legalName: null,
          recps: true
        }
      },
      {
        type: 'person',
        details: {
          preferredName: null,
          legalName: 'Cherese Eriepa',
          recps: true
        }
      },
      {
        type: 'person',
        details: {
          preferredName: 'Cher',
          altNames: null,
          recps: true
        }
      },
      {
        type: 'person',
        details: {
          preferredName: 'Reese',
          altNames: {
            add: ['Cherese']
          },
          recps: true
        }
      },
      {
        type: 'person',
        details: {
          preferredName: 'Cher',
          altNames: {
            add: [null] // unsure if this is possible, but lets test it
          },
          recps: true
        }
      }
    ],
    (server) => {
      server.profile.find({ type: 'person', name: 'Cher' }, (err, profiles) => {
        t.error(err, 'Run find for Cherese on profiles with empty fields')

        t.equals(profiles.length, 5, 'returns all 5 profiles')

        t.true(
          profiles.every(p => (
            (p.preferredName && p.preferredName.match(/Cher/i)) ||
            (p.legalName && p.legalName.match(/Cher/i)) ||
            (p.altNames && p.altNames.find(name => name.match(/Cher/i)))
          )) && profiles.length !== 0,
          'all results contain search term "Cher"'
        )
        server.close()
      })
    }
  )
})

test('find - is it fast', t => {
  t.plan(3)
  const N = 1_000

  const input = new Array(N).fill().map((el, i) => {
    if (i !== 100) {
      return {
        type: 'person',
        details: { preferredName: 'dave' }
      }
    }
    return {
      type: 'person',
      details: { preferredName: 'ben tai' }
    }
  })

  let time = Date.now()
  setup(
    input,
    (server) => {
      t.pass(`write ${N} records: ${prettyTime(Date.now() - time)}`)

      time = Date.now()
      server.profile.find({ type: 'person', name: 'ben' }, (err, profiles) => {
        if (err) console.log(err)
        t.equal(profiles.length, 1, 'finds ben ' + prettyTime(Date.now() - time))

        time = Date.now()
        server.profile.find({ type: 'person', name: 'ben' }, (err, profiles) => {
          if (err) console.log(err)
          t.equal(profiles.length, 1, 'finds ben ' + prettyTime(Date.now() - time))
          server.close()
        })
      })
    }
  )
})

function prettyTime (t) {
  return '(' + t + 'ms)'
}
