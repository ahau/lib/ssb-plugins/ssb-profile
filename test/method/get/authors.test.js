const test = require('tape')
const Server = require('../../test-bot')
const { replicate } = require('scuttle-testbot')

// TODO extract into an authors module

test('get (non-authors cant edit)', t => {
  const me = Server()
  const friend = Server()

  //    A (root)
  //    |
  //    B

  const edit = {
    preferredName: 'michael',
    authors: {
      add: [me.id]
    }
  }

  me.profile.publicPerson.create(edit, (err, profileId) => {
    t.error(err, 'create profile')

    const invalidUpdate = {
      preferredName: 'dave'
    }

    replicate({ from: me, to: friend }, (err) => {
      t.error(err, 'replicate from me to friend')

      friend.profile.publicPerson.update(profileId, invalidUpdate, (err) => {
        t.match(err.message, /Invalid author/)

        const invalidUpdate2 = {
          type: 'profile/person',
          preferredName: { set: 'david' },
          tangles: {
            profile: { root: profileId, previous: [profileId] }
          }
        }

        friend.db.create({ content: invalidUpdate2 }, err => {
          if (err) throw err

          replicate({ from: friend, to: me }, (err) => {
            if (err) throw err

            me.profile.publicPerson.get(profileId, (err, profile) => {
              t.error(err, 'get profile')

              t.equal(
                profile.preferredName,
                'michael',
                'unauthorised updates are ignored'
              )

              me.close()
              friend.close()
              t.end()
            })
          })
        })
      })
    })
  })
})

test('get (authors)', t => {
  const me = Server()
  const friend = Server()

  //    A (root)
  //    |
  //    B

  const edit = {
    preferredName: 'michael',
    authors: {
      add: [
        me.id,
        friend.id
      ]
    }
  }

  me.profile.publicPerson.create(edit, (err, profileId) => {
    t.error(err, 'create profile')

    const validUpdate = {
      preferredName: 'dave'
    }

    replicate({ from: me, to: friend }, (err) => {
      if (err) throw err

      friend.profile.publicPerson.update(profileId, validUpdate, (err) => {
        t.error(err, 'friend publishes an update')
        // if (err) throw err

        replicate({ from: friend, to: me }, (err) => {
          // if (err) throw err
          t.error(err, 'friend replicates update to me')

          me.profile.publicPerson.get(profileId, (err, profile) => {
            t.error(err, 'get profile')

            t.equal(
              profile.preferredName,
              'dave',
              'authorised updates from others are included'
            )

            me.close()
            friend.close()
            t.end()
          })
        })
      })
    })
  })
})

test('author (all authors)', t => {
  const me = Server()
  const friend = Server()

  //    A (root)
  //    |
  //    B

  const edit = {
    preferredName: 'michael',
    authors: {
      add: [
        me.id,
        '*'
      ]
    }
  }

  // i create a record
  me.profile.publicPerson.create(edit, (err, profileId) => {
    t.error(err, 'create profile')

    const validUpdate = {
      preferredName: 'dave'
    }

    replicate({ from: me, to: friend }, (err) => {
      if (err) throw err

      // friend updates the record
      friend.profile.publicPerson.update(profileId, validUpdate, (err) => {
        t.error(err, 'friend publishes an update removing me as an author')
        // if (err) throw err

        replicate({ from: friend, to: me }, (err) => {
          if (err) throw err

          me.profile.publicPerson.get(profileId, (err, profile) => {
            t.error(err, 'get profile')

            t.equal(
              profile.preferredName,
              'dave',
              'all authors - authorised updates from others are included'
            )

            me.close()
            friend.close()
            t.end()
          })
        })
      })
    })
  })
})

test('author (add + remove)', t => {
  const me = Server()
  const friend = Server()

  const details = {
    preferredName: 'Dayn',
    authors: {
      add: [me.id, friend.id]
    }
  }

  me.profile.publicPerson.create(details, (err, profileId) => {
    t.error(err, 'create a profile and add friend as an author')

    replicate({ from: me, to: friend }, (err) => {
      t.error(err, 'replicate the record to friend')

      friend.profile.publicPerson.update(profileId, { preferredName: 'Daynes' }, (err) => {
        t.error(err, 'friend can update that record')

        replicate({ from: friend, to: me }, (err) => {
          t.error(err, 'friend replicates their update to me')

          const removeAuthorUpdate = {
            authors: {
              add: [],
              remove: [friend.id]
            }
          }
          me.profile.publicPerson.update(profileId, removeAuthorUpdate, (err) => {
            t.error(err, 'remove friend as an author')

            replicate({ from: me, to: friend }, (err) => {
              t.error(err, 'replicate the update to friend')

              friend.profile.publicPerson.update(profileId, { preferredName: 'Daynah' }, (err) => {
                t.match(err.message, /Invalid author/)

                me.close()
                friend.close()
                t.end()
              })
            })
          })
        })
      })
    })
  })
})

test('author (add all authors + friend, then remove friend)', t => {
  const me = Server()
  const friend = Server()

  const details = {
    preferredName: 'Dayn',
    authors: {
      add: [friend.id, '*']
    }
  }

  me.profile.publicPerson.create(details, (err, profileId) => {
    t.error(err, 'create a profile and add friend as an author')

    replicate({ from: me, to: friend }, (err) => {
      t.error(err, 'replicate the record to friend')

      friend.profile.publicPerson.update(profileId, { preferredName: 'Daynes' }, (err) => {
        t.error(err, 'friend can update that record')

        replicate({ from: friend, to: me }, (err) => {
          t.error(err, 'friend replicates their update to me')

          const removeAuthorUpdate = {
            authors: {
              add: [],
              remove: [friend.id]
            }
          }
          me.profile.publicPerson.update(profileId, removeAuthorUpdate, (err) => {
            t.error(err, 'remove friend as an author')

            replicate({ from: me, to: friend }, (err) => {
              t.error(err, 'replicate the update to friend')

              friend.profile.publicPerson.update(profileId, { preferredName: 'Daynah' }, (err) => {
                t.error(err, 'friend updates the record after being removed as an author')

                replicate({ from: friend, to: me }, (err) => {
                  t.error(err, 'friend replicates the authorised edit to me')

                  me.profile.publicPerson.get(profileId, (err, profile) => {
                    t.error(err, 'get the profile')
                    t.equal(
                      profile.preferredName,
                      'Daynah',
                      'returns friends authorised update'
                    )

                    me.close()
                    friend.close()
                    t.end()
                  })
                })
              })
            })
          })
        })
      })
    })
  })
})

// TODO split out to get test ?
test('author (no authors)', t => {
  const me = Server()

  const edit = {
    preferredName: 'michael'
  }

  me.profile.publicPerson.create(edit, (err) => {
    t.match(err.message, /Invalid initial authors/)

    edit.authors = {
      remove: [me.id]
    }

    me.profile.publicPerson.create(edit, (err) => {
      t.match(err.message, /Invalid initial authors/)

      t.end()
      me.close()
    })
  })
})

test('author (original author can be removed)', t => {
  const me = Server()
  const friend = Server()

  const details = {
    preferredName: 'Dayn',
    authors: {
      add: [friend.id] // me is not listed as an author
    }
  }
  friend.close()

  me.profile.publicPerson.create(details, (err, profileId) => {
    t.error(err, 'create a profile and add friend as an author')

    me.profile.publicPerson.update(profileId, { preferredName: 'Daynes' }, (err) => {
      t.match(err.message, /Invalid author/)

      me.close()
      t.end()
    })
  })
})

/*
  1. create a profile with 1 author
  2. manually publish an invalid message which would remove that 1 author
  3. get the profile
    -> should ignore the invalid message
*/
test('author (#get ignores any "remove-last author" change)', t => {
  const me = Server()

  const details = {
    authors: {
      add: [me.id]
    }
  }
  me.profile.publicPerson.create(details, (err, profileId) => {
    t.error(err, 'create profile with 1 author')

    const content = {
      type: 'profile/person',
      authors: {
        [me.id]: { 7: -1 }
      },
      tangles: {
        profile: {
          root: profileId,
          previous: [profileId]
        }
      }
    }

    // TODO add this back in !
    // t.true(isUpdate(content))

    me.db.create({ content }, (err, msg) => {
      t.error(err, 'publish invalid update')

      me.profile.publicPerson.get(profileId, (err, profile) => {
        t.error(err, 'get profile')

        t.deepEqual(
          profile.authors,
          {
            [me.id]: [{ start: 0, end: null }]
          }
        )
        t.end()
        me.close()
      })
    })
  })
})
test('author (#update blocks any "remove-last author" change)', t => {
  const me = Server()

  const details = {
    authors: { add: [me.id] }
  }
  me.profile.publicPerson.create(details, (err, profileId) => {
    t.error(err, 'create profile with 1 author')

    const update = {
      authors: { remove: [me.id] }
    }
    me.profile.publicPerson.update(profileId, update, (err) => {
      t.match(err.message, /Invalid authors change/)
      t.end()
      me.close()
    })
  })
})

// WIP - i modified the checker isValidNextStep and it's ... not working here any more
// npm link ssb-crut

test('authors (old root message missing authors)', t => {
  const server = Server()

  const initialProfile = {
    type: 'profile/person',
    preferredName: { set: 'cherese' },
    tangles: {
      profile: { root: null, previous: null }
    }
    // NOTE: we are not setting the authors on this message
  }

  server.db.create({ content: initialProfile }, (_, root) => {
    t.false(root.value.content.authors, 'an "old style" authorless root now exists')

    server.profile.publicPerson.get(root.key, (err, profile) => {
      t.error(err, 'gets profile without errors')

      t.deepEqual(
        profile.authors,
        {
          [server.id]: [// because theyre the original author
            { start: root.value.sequence, end: null } // uses the sequence number of the message
          ]
        }
      )

      const someone = '@boN8vDUEPWKkUhISoBks50BKDSx6r/fxQ2bcA4fqSbk=.ed25519'
      server.profile.publicPerson.update(root.key, { authors: { add: [someone] } }, (err) => {
        t.error(err, 'can publish update, changing authors')

        server.profile.publicPerson.get(root.key, (_, profile) => {
          t.deepEqual(
            profile.authors,
            {
              [server.id]: [// because theyre the original author
                { start: root.value.sequence, end: null } // uses the sequence number of the message
              ],
              [someone]: [
                { start: 0, end: null }
              ]
            }
          )

          server.close()
          t.end()
        })
      })
    })
  })
})
