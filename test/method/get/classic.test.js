const test = require('tape')
const pull = require('pull-stream')
const Server = require('../../test-bot')

function publish ({ server, lookup, previousMap }) {
  return function (keys, hooks = [], cb) {
    pull(
      pull.values(keys),
      pull.asyncMap((key, cb) => {
        let content = lookup[key]

        hooks.forEach(hook => {
          content = hook(key, content, { server, lookup, previousMap })
        })

        const pub = (content, cb) => content.recps ? server.tribes.publish(content, cb) : server.db.create({ content }, cb)
        pub(content, (err, msg) => {
          if (err) return cb(err)

          lookup[key] = msg
          cb(null, msg)
        })
      }),
      pull.collect(cb)
    )
  }
}

function addTangle (key, content, { server, lookup, previousMap }) {
  const previous = previousMap[key]

  if (!previous) {
    content.tangles = {
      profile: { root: null, previous: null }
    }
    return content
  }

  content.tangles = {
    profile: {
      root: lookup.A.key, // always the same
      previous: Object.keys(previous).map(k => {
        const partentKey = lookup[k].key
        if (!partentKey) throw Error(`looks like ${k} hasn't been published yet!`)

        return partentKey
      })
    }
  }

  return content
}

// function addRecps (key, content, { server, lookup, previousMap }) {
//   content.recps = [server.id]
//   return content
// }

// TODO change to use #create + #update to publish updates
// this will make tests less bittle to any changes in format?
test('get', t => {
  const server = Server({ tribes: true })

  //    A (root)
  //    |
  //    B

  const previousMap = {
    B: { A: 1 }
  }
  // describes the previous of each message (called revereMap in graph work)

  const lookup = {
    A: {
      type: 'profile/person',
      preferredName: { set: 'Taika' },
      legalName: { set: 'xxxxx' }, // should not be on publicProfile
      // description: { set: 'traditional bases in the taranaki + wellington regions' },
      // altNames: { wellington: 1, wairarapa: 1 },
      avatarImage: {
        set: {
          blob: '&1ZQM7TjQHBUEcdBijB6y7dkX047wCf4aXcjFplTjrJo=.sha256',
          unbox: 'YmNz1XfPw/xkjoN594ZfE/JUhpYiGyOOQwNDf6DN+54=.boxs',
          mimeType: 'image/png'
        }
      },

      gender: { set: 'male' },
      authors: { [server.id]: { 1: 1 } }
      // tangles: {
      //   profile: { root: null, previous: null }
      // }
    },
    B: {
      type: 'profile/person',
      preferredName: { set: 'Taika Waititi' }, // chang to full name
      authors: { '*': { 12345: 1 } }
      // tangles: {
      //   profile: { root: '?', previous: ['?'] }
      // }
    }
  }
  // a lookup which maps our easy message labels to content we want published
  // after-publish, the content is replaced with actual ssb message

  publish({ server, lookup, previousMap })(
    ['A', 'B'], // publish order could be derived from previousMap, but this is fine
    [addTangle],
    (err) => {
      if (err) throw err

      const profileId = lookup.A.key
      server.profile.publicPerson.get(profileId, (err, actual) => {
        if (err) throw err

        const state = {
          preferredName: 'Taika Waititi',
          gender: 'male',
          avatarImage: {
            blob: '&1ZQM7TjQHBUEcdBijB6y7dkX047wCf4aXcjFplTjrJo=.sha256',
            unbox: 'YmNz1XfPw/xkjoN594ZfE/JUhpYiGyOOQwNDf6DN+54=.boxs',
            mimeType: 'image/png'
          },
          authors: {
            [server.id]: [{ start: 1, end: null }],
            '*': [{ start: 12345, end: null }]
          },

          tombstone: null
        }

        const expected = {
          key: profileId,
          type: 'person',
          originalAuthor: server.id,
          recps: null,
          ...state,
          conflictFields: [],
          states: []
        }
        // #get returns an object which maps the current heads to the
        // reified transformations at those heads
        //
        // here B is the current singular head

        t.deepEqual(actual, expected, 'get resolve real state')

        server.close()
        t.end()
      })
    }
  )
})

test('get (private)', t => {
  const server = Server({ tribes: true })

  server.tribes.create({}, (err, { groupId } = {}) => {
    if (err) throw err

    //    A (root)
    //    |
    //    B

    const previousMap = {
      B: { A: 1 }
    }
    // describes the previous of each message (called revereMap in graph work)

    const lookup = {
      A: {
        type: 'profile/person',
        preferredName: { set: 'Te Ati Awa' },
        description: { set: 'traditional bases in the taranaki + wellington regions' },
        altNames: { wellington: 1, wairarapa: 1 },
        authors: { [server.id]: { 1: 1 } },
        school: {
          set: [
            'My High School',
            'Some Other School'
          ]
        },
        // tangles: {
        //   profile: { root: null, previous: null }
        // },
        recps: [groupId]
      },
      B: {
        type: 'profile/person',
        preferredName: { set: 'Te Āti Awa' }, // fixes a typo
        // description: no change
        altNames: { wairarapa: -1, taranaki: 1 },
        authors: { [server.id]: { 900: 1 } },
        school: {
          set: [
            'My High School',
            'Some Other School',
            'And the other one'
          ]
        },
        // tangles: {
        //   profile: { root: '?', previous: ['?'] }
        // },
        recps: [groupId]
      }
    }
    // a lookup which maps our easy message labels to content we want published
    // after-publish, the content is replaced with actual ssb message

    publish({ server, lookup, previousMap })(
      ['A', 'B'], // publish order could be derived from previousMap, but this is fine
      [addTangle],
      () => {
        const profileId = lookup.A.key

        server.profile.privatePerson.get(profileId, (err, actual) => {
          if (err) throw err

          const state = {
            preferredName: 'Te Āti Awa',
            legalName: null,
            altNames: ['taranaki', 'wellington'],
            authors: {
              [server.id]: [
                { start: 1, end: null }
              ]
            },
            description: 'traditional bases in the taranaki + wellington regions',
            gender: null,
            source: null,
            aliveInterval: null,
            birthOrder: null,
            deceased: null,
            placeOfBirth: null,
            placeOfDeath: null,
            buriedLocation: null,
            avatarImage: null,
            headerImage: null,
            customFields: {},

            city: null,
            country: null,
            postCode: null,

            profession: null,
            education: [],
            school: [
              'My High School',
              'Some Other School',
              'And the other one'
            ],

            tombstone: null
          }
          const expected = {
            key: profileId,
            type: 'person',
            originalAuthor: server.id,
            recps: [groupId],
            ...state,
            conflictFields: [],
            states: []
          }
          // #get returns an object which maps the current heads to the
          // reified transformations at those heads
          //
          // here B is the current singular head
          t.deepEqual(actual, expected, 'get resolve real state')

          server.close()
          t.end()
        })
      }
    )
  })
})

test('get (tombstoned!)', t => {
  const server = Server({ tribes: true })

  //    A (root)
  //    |
  //    B

  const previousMap = {
    B: { A: 1 }
  }
  // describes the previous of each message (called revereMap in graph work)
  const tombstoneDate = Number(new Date('1902-04-04'))
  // const lookup = {

  //   B:
  // }

  const A = {
    type: 'profile/person',
    preferredName: { set: 'Te Ati Awa' },
    // tangles: {
    //   profile: { root: null, previous: null }
    // },
    authors: { [server.id]: { 1: 1 } }
  }

  const B = {
    type: 'profile/person',
    preferredName: { set: 'TE ATI AWA' },
    tombstone: {
      set: { date: tombstoneDate, reason: 'mistake' }
    },
    // tangles: {
    //   profile: { root: '?', previous: ['?'] }
    // },
    authors: { [server.id]: { 1: 1 } }
  }

  const lookup = {
    A,
    B
  }
  // a lookup which maps our easy message labels to content we want published
  // after-publish, the content is replaced with actual ssb message

  publish({ server, lookup, previousMap })(
    ['A', 'B'], // publish order could be derived from previousMap, but this is fine
    [addTangle],
    () => {
      const profileId = lookup.A.key

      server.profile.person.public.get(profileId, (_, actual) => {
        const state = {
          preferredName: 'TE ATI AWA',
          avatarImage: null,
          gender: null,
          authors: {
            [server.id]: [{ start: 1, end: null }]
          },

          tombstone: { date: tombstoneDate, reason: 'mistake' }
        }
        const expected = {
          key: profileId,
          type: 'person',
          originalAuthor: server.id,
          recps: null,
          ...state,
          conflictFields: [],
          states: []
        }
        // #get returns an object which maps the current heads to the
        // reified transformations at those heads
        //
        // here B is the current singular head

        t.deepEqual(actual, expected, 'get resolve real state')

        server.close()
        t.end()
      })
    }
  )
})

test('get (create+update integration)', t => {
  const server = Server({ tribes: true })
  server.tribes.create({}, (_, { groupId } = {}) => {
    const friendId = '@xIP5FV16FwPUiIZ0TmINhoCo4Hdx6c4KQcznEDeWtWg=.ed25519'

    const initial = {
      preferredName: 'Te Ati Awa',
      altNames: { add: ['taranaki', 'WELLINGTON'] },
      authors: { add: [server.id] },
      description: 'traditional bases in the taranaki + wellington regions',
      recps: [groupId]
    }
    const update = {
      preferredName: 'Te Āti Awa',
      altNames: {
        add: ['Wellington'],
        remove: ['WELLINGTON']
      },
      authors: {
        add: [friendId],
        remove: [server.id]
      },
      birthOrder: 4
    }

    server.profile.person.group.create(initial, (err, profileId) => {
      t.error(err, 'creates without error')

      server.profile.person.group.update(profileId, update, (err, updateId) => {
        t.error(err, 'updates without error')

        server.profile.person.group.get(profileId, (err, profile) => {
          t.error(err, 'gets without error')

          const actual = profile
          const expected = {
            key: profileId,
            originalAuthor: server.id,
            type: 'person',
            preferredName: 'Te Āti Awa',
            altNames: ['taranaki', 'Wellington'].sort(),
            authors: {
              [server.id]: [{ start: 2, end: 3 }],
              [friendId]: [{ start: 0, end: null }]
            },
            description: 'traditional bases in the taranaki + wellington regions',
            birthOrder: 4,

            legalName: null,
            gender: null,
            source: null,
            city: null,
            country: null,
            postCode: null,
            aliveInterval: null,
            tombstone: null,
            profession: null,
            education: [],
            school: [],
            avatarImage: null,
            headerImage: null,
            deceased: null,
            placeOfBirth: null,
            placeOfDeath: null,
            buriedLocation: null,
            customFields: {},
            recps: [groupId],
            conflictFields: [],
            states: []
          }

          t.deepEqual(actual, expected, 'update worked!')

          server.close()
          t.end()
        })
      })
    })
  })
})
