const test = require('tape')
const pull = require('pull-stream')
const Server = require('../../test-bot')

/* NOTE
 * - this does not yet test allowPublic
 */

function run (opts = {}, next) {
  if (typeof opts === 'function') return run({}, opts)
  const server = Server(Object.assign(opts, { tribes: true }))

  const {
    makeGroup,
    makeProfile
  } = opts

  const state = {
    profileId: null,
    profileRecps: null,
    groupId: null
  }

  pull(
    pull.once(opts),
    pull.asyncMap((opts, cb) => {
      if (!makeGroup) return cb(null, opts)

      server.tribes.create({}, (err, { groupId }) => {
        if (err) return cb(err)
        state.groupId = groupId
        state.profileRecps = [groupId]
        cb(null, opts)
      })
    }),
    pull.asyncMap((opts, cb) => {
      if (!makeProfile) return cb(null, opts)

      const details = {
        preferredName: 'Te Ati Awa',
        authors: {
          add: [server.id]
        }
      }
      if (state.profileRecps) {
        details.recps = state.profileRecps
      }

      const crut = state.profileRecps
        ? server.profile.privatePerson
        : server.profile.publicPerson

      crut.create(details, (err, profileId) => {
        if (err) return cb(err)
        state.profileId = profileId
        cb(null, opts)
      })
    }),
    pull.collect((err) => {
      if (err) throw err

      next({ server, ...state })
    })
  )
}

test('link.create (feed-profile) LEGACY', t => {
  // /////////
  t.plan(6)
  // /////////

  // linking to a valid profile
  run({ makeProfile: true }, ({ server, profileId }) => {
    server.profile.link.create({ profile: profileId }, (err, link) => {
      t.error(err, 'create link without error')

      const { parent: _feedId, child: _profileId } = link.value.content

      t.equal(_feedId, server.id, 'encodes current feedId')
      t.equal(_profileId, profileId, 'encodes the right profile')

      server.close()
    })
  })

  // link to a private profile!
  run({ makeGroup: true, makeProfile: true }, ({ server, profileId, profileRecps }) => {
    server.profile.link.create({ profile: profileId }, (err, link) => {
      t.error(err, 'does not error when linking to a private profile')

      server.get({ id: link.key, private: true, meta: true }, (_, data) => {
        t.deepEqual(data.value.content.recps, profileRecps, 'has same recps on link as profile')

        server.close()
      })
    })
  })

  // trying to link to a post!
  run(({ server }) => {
    server.db.create({ content: { type: 'post', text: 'I is me!' } }, (_, msg) => {
      server.profile.link.create({ profile: msg.key }, (err, link) => {
        t.notEqual(err, null, 'errors when linking to some non-profile msg')

        server.close()
      })
    })
  })
})

test('link.create (group-profile) LEGACY', t => {
  // /////////
  t.plan(9)
  // /////////

  const parentGroupId = '%qBzOO6JC8OkvwmZA9UzAfSMWGVxXtv7BhaHDAlM+/so=.cloaked'

  // linking to a valid profile
  run({ makeGroup: true, makeProfile: true }, ({ server, groupId, profileId }) => {
    const details = {
      type: 'group-profile',
      profile: profileId,
      group: groupId,
      parentGroupId
    }

    server.profile.link.create(details, (err, link) => {
      t.error(err, 'creates link without error')

      server.get({ id: link.key, private: true, meta: true }, (_, link) => {
        const {
          parent: _groupId,
          child: _profileId,
          recps,
          parentGroupId: _parentGroupId
        } = link.value.content

        t.equal(_groupId, groupId, 'encodes right groupId')
        t.equal(_profileId, profileId, 'encodes the right profile')
        t.equal(_parentGroupId, parentGroupId, 'encodes the right parent group id')
        t.deepEqual(recps, [groupId], 'encoded to group')

        server.close()
      })
    })
  })

  // linking to link group to a public profile (publicly)
  run({ makeProfile: true }, ({ server, profileId }) => {
    server.tribes.create({}, (_, { groupId }) => {
      const details = {
        type: 'group-profile',
        profile: profileId,
        group: groupId
      }

      server.profile.link.create(details, (err, link) => {
        t.error(err)

        server.get({ id: link.key, meta: true, private: false }, (_, link) => {
          t.equal(typeof link.value.content, 'object', 'can link public profile and group (publicly)')
          t.equal(link.value.content.recps, undefined, 'no recps, not encrypted')

          server.close()
        })
      })
    })
  })

  // trying to link group to a private profile that is not in group
  run({ makeGroup: true, makeProfile: true }, ({ server, profileId }) => {
    // make **another** different group than the profile is in
    server.tribes.create({}, (_, { groupId }) => {
      const details = {
        type: 'group-profile',
        profile: profileId,
        group: groupId
      }

      server.profile.link.create(details, (err) => {
        t.error(err, 'create link a profile not in the group')

        server.close()
      })
    })
  })
})
