const test = require('tape')
const pull = require('pull-stream')
const { promisify: p } = require('util')
const Server = require('../../test-bot')
const { isMsgId } = require('ssb-ref')

/* NOTE
 * - this does not yet test allowPublic
 */

function setup (opts = {}, next) {
  if (typeof opts === 'function') return setup({}, opts)
  const server = Server(Object.assign(opts, { tribes: true }))

  const {
    makeGroup,
    makeProfile
  } = opts

  const state = {
    profileId: null,
    profileRecps: null,
    groupId: null
  }

  pull(
    pull.once(opts),
    pull.asyncMap((opts, cb) => {
      if (!makeGroup) return cb(null, opts)

      server.tribes.create({}, (err, { groupId }) => {
        if (err) return cb(err)
        state.groupId = groupId
        state.profileRecps = [groupId]
        cb(null, opts)
      })
    }),
    pull.asyncMap((opts, cb) => {
      if (!makeProfile) return cb(null, opts)

      const details = {
        preferredName: 'Te Ati Awa',
        authors: {
          add: [server.id]
        }
      }
      if (state.profileRecps) {
        details.recps = state.profileRecps
      }

      const crut = state.profileRecps
        ? server.profile.privatePerson
        : server.profile.publicPerson

      crut.create(details, (err, profileId) => {
        if (err) return cb(err)
        state.profileId = profileId
        cb(null, opts)
      })
    }),
    pull.collect((err) => {
      if (err) throw err

      next({ server, ...state })
    })
  )
}

test('link.create (feed-profile)', t => {
  // /////////
  t.plan(9)
  // /////////

  // linking to a valid profile
  setup({ makeProfile: true }, ({ server, profileId }) => {
    server.profile.link.create(profileId, (err, link) => {
      t.error(err)
      const { parent: _feedId, child: _profileId } = link.value.content

      t.equal(_feedId, server.id, 'encodes current feedId')
      t.equal(_profileId, profileId, 'encodes the right profile')

      server.close()
    })
  })

  // linking to a valid profile, with another feedId
  setup({ makeProfile: true }, ({ server, profileId }) => {
    const daveId = server.id.replace(/\w{4}/, 'dave') // lol

    server.profile.link.create(profileId, { feedId: daveId }, (err, link) => {
      t.error(err)
      const { parent: _feedId, child: _profileId } = link.value.content

      t.equal(_feedId, daveId, 'can encode a custom feedId')
      t.equal(_profileId, profileId, 'encodes the right profile')

      server.close()
    })
  })

  // link to a private profile!
  setup({ makeGroup: true, makeProfile: true }, ({ server, profileId, profileRecps }) => {
    server.profile.link.create(profileId, (err, link) => {
      t.equal(err, null, 'does not error when linking to a private profile')
      server.get({ id: link.key, private: true, meta: true }, (_, data) => {
        t.deepEqual(data.value.content.recps, profileRecps, 'has same recps on link as profile')

        server.close()
      })
    })
  })

  // trying to link to a post!
  setup(({ server }) => {
    server.db.create({ content: { type: 'post', text: 'I is me!' } }, (_, msg) => {
      server.profile.link.create(msg.key, (err, link) => {
        t.notEqual(err, null, 'errors when linking to some non-profile msg')

        server.close()
      })
    })
  })
})

test('link.create (group-profile)', t => {
  // /////////
  t.plan(9)
  // /////////

  const parentGroupId = '%qBzOO6JC8OkvwmZA9UzAfSMWGVxXtv7BhaHDAlM+/so=.cloaked'

  // linking to a valid profile
  setup({ makeGroup: true, makeProfile: true }, ({ server, groupId, profileId }) => {
    server.profile.link.create(profileId, { groupId, parentGroupId }, (err, link) => {
      t.error(err, 'creates link')

      server.get({ id: link.key, private: true, meta: true }, (_, link) => {
        const {
          parent: _groupId,
          child: _profileId,
          parentGroupId: _parentGroupId,
          recps
        } = link.value.content

        t.equal(_groupId, groupId, 'encodes right groupId')
        t.equal(_profileId, profileId, 'encodes the right profile')
        t.equal(_parentGroupId, parentGroupId, 'encodes the right parent group id')
        t.deepEqual(recps, [groupId], 'encoded to group')

        server.close()
      })
    })
  })

  // linking to link group to a public profile (publicly)
  setup({ makeProfile: true }, ({ server, profileId }) => {
    server.tribes.create({}, (_, { groupId }) => {
      server.profile.link.create(profileId, { groupId }, (err, link) => {
        t.error(err)

        server.get({ id: link.key, meta: true, private: false }, (_, link) => {
          t.equal(typeof link.value.content, 'object', 'can link public profile and group (publicly)')
          t.equal(link.value.content.recps, undefined, 'no recps, not encrypted')

          server.close()
        })
      })
    })
  })

  // trying to link group to a private profile that is not in group
  setup({ makeGroup: true, makeProfile: true }, ({ server, profileId }) => {
    // make **another** different group than the profile is in
    server.tribes.create({}, (_, { groupId }) => {
      server.profile.link.create(profileId, { groupId }, (err) => {
        t.error(err, 'create link a profile not in the group')

        server.close()
      })
    })
  })
})

test('link.create (parentGroupId for subgroup link/group-profile)', async t => {
  // /////////
  t.plan(4)
  // /////////
  const server = Server({ tribes: true })

  const createLink = (profileId, opts) => p(server.profile.link.create)(profileId, opts)
  const createProfile = (input) => p(server.profile.privateCommunity.create)({ ...input, authors: { add: [server.id] } })

  // create parent group
  const { groupId } = await p(server.tribes.create)({})

  // create profile for the parent group
  const profileId = await createProfile({ recps: [groupId] })
  t.true(isMsgId(profileId), 'creates group profile')

  // link profile to parent group
  await createLink(profileId, { groupId })

  //
  // repeat above for subgroup
  //
  // create the subgroup in the group
  const { groupId: subgroupId } = await p(server.tribes.create)({}) // fake it till we can make subgroups

  // create profile for the subgroup
  const subgroupProfileId = await createProfile({ recps: [subgroupId] })
  t.true(isMsgId(subgroupProfileId), 'creates subgroup profile')

  // link profile to subgroup
  const subgroupLink = await createLink(subgroupProfileId, { groupId: subgroupId, parentGroupId: groupId })

  let content = {
    key: subgroupLink.key,
    type: 'link/group-profile',
    originalAuthor: server.id,
    recps: [subgroupId],
    parent: subgroupId,
    child: subgroupProfileId,
    parentGroupId: groupId,
    tombstone: null,
    conflictFields: [],
    states: []
  }

  t.deepEqual(
    subgroupLink,
    {
      ...content,
      // legacy
      value: { content }
    },
    'subgroup link created'
  )

  // /////////
  // need to test when the API is used differently
  // ////////
  const { groupId: parentGroupId } = await p(server.tribes.create)({})

  const profile2Id = await createProfile({ recps: [groupId] })
  const link = await p(server.profile.link.create)({ profile: profile2Id, group: groupId, parentGroupId })

  content = {
    key: link.key,
    type: 'link/group-profile',
    originalAuthor: server.id,
    recps: [groupId],
    parent: groupId,
    child: profile2Id,
    parentGroupId,
    tombstone: null,
    conflictFields: [],
    states: []
  }
  t.deepEqual(
    link,
    {
      ...content,
      /* legacy */
      value: { content }
    },
    'returns matching link'
  )

  server.close()
})
