const { isMsgId } = require('ssb-ref')
const test = require('tape')
const { promisify: p } = require('util')

const Server = require('../../test-bot')

const emptyProfileFields = {
  legalName: null,
  altNames: [],
  description: null,
  city: null,
  country: null,
  postCode: null,
  profession: null,
  education: [],
  school: [],
  gender: null,
  source: null,
  aliveInterval: null,
  deceased: null,
  placeOfBirth: null,
  placeOfDeath: null,
  buriedLocation: null,
  birthOrder: null,
  avatarImage: null,
  headerImage: null,
  tombstone: null,

  phone: null,
  address: null,
  email: null,
  customFields: {}
}

test('ssb.profile.person.admin.create (recps)', async t => {
  const server = Server({ tribes: true })
  const feedId = server.id
  const { groupId, poBoxId } = await p(server.tribes.create)({ addPOBox: true })

  async function isHappyRecps (isHappy, recps) {
    const input = {
      authors: {
        add: [server.id]
      },
      recps
    }
    const failMsg = [
      isHappy ? 'should pass:' : 'should fail',
      recps === undefined ? 'undefined' : JSON.stringify(recps)
    ].join(' ')

    const profile = await p(server.profile.person.admin.create)(input)
      .catch(err => {
        if (isHappy) t.fail(failMsg)
        else t.true(err, 'bad recps blocked')
      })

    if (profile) {
      if (isHappy) t.true(profile, recps)
      else t.fail(failMsg)
    }
  }

  await isHappyRecps(true, [groupId])
  await isHappyRecps(true, [poBoxId])
  await isHappyRecps(true, [poBoxId, feedId])

  await isHappyRecps(false, undefined)
  await isHappyRecps(false, [])
  await isHappyRecps(false, [feedId])
  await isHappyRecps(false, [feedId, poBoxId])
  await isHappyRecps(false, [poBoxId, feedId, feedId])

  server.close(t.end)
})

test('ssb.profile.person.admin.create (unallowed input)', t => {
  const server = Server({ tribes: true })
  server.tribes.create({ addPOBox: true }, (_, { poBoxId } = {}) => {
    const input = {
      authors: {
        add: [server.id]
      },
      dog: '022 0220 2222',
      recps: [poBoxId, server.id]
    }

    server.profile.person.admin.create(input, (err) => {
      t.match(err.message, /unallowed inputs: dog/, 'doesnt allow additional props')

      server.close(t.end)
    })
  })
})

test('ssb.profile.person.admin.*', { objectPrintDepth: 10 }, t => {
  const server = Server({ tribes: true })
  server.tribes.create({ addPOBox: true }, (_, { poBoxId } = {}) => {
    const input = {
      preferredName: 'Maui',
      legalName: 'Maui Hangarau',
      source: 'webForm',
      authors: {
        add: [server.id]
      },
      recps: [poBoxId, server.id]
    }

    server.profile.person.admin.create(input, (err, profileId) => {
      t.error(err, 'creates a admin person profile')

      t.true(isMsgId(profileId), 'returns valid profileId')
      server.profile.person.admin.get(profileId, (err, profile) => {
        t.error(err, 'gets admin person profile')

        const state = {
          ...emptyProfileFields,
          tombstone: null,
          preferredName: 'Maui',
          legalName: 'Maui Hangarau',
          source: 'webForm',
          authors: {
            [server.id]: [{ start: 3, end: null }] // prior messages: group/init, group/po-box
          }
        }
        t.deepEqual(
          profile,
          {
            key: profileId,
            type: 'person/admin',
            originalAuthor: server.id,
            recps: [poBoxId, server.id],
            ...state,
            conflictFields: [],
            states: []
          },
          'profile state is correct'
        )

        server.profile.person.admin.update(profileId, { gender: 'other' }, (err, updateId) => {
          t.error(err, 'updates the profile')

          server.profile.person.admin.get(profileId, (err, updatedProfile) => {
            t.error(err, 'gets the updated profile')

            const state = {
              ...emptyProfileFields,
              preferredName: 'Maui',
              legalName: 'Maui Hangarau',
              gender: 'other',
              source: 'webForm',
              authors: {
                [server.id]: [{ start: 3, end: null }]
              }
            }
            t.deepEqual(
              updatedProfile,
              {
                key: profileId,
                type: 'person/admin',
                originalAuthor: server.id,
                recps: [poBoxId, server.id],
                ...state,
                conflictFields: [],
                states: []
              },
              'returns the updated profile'
            )

            server.profile.person.admin.tombstone(profileId, { reason: 'user deleted account' }, (err, tombstoneId) => {
              t.error(err, 'tombstones profile')

              server.profile.person.admin.get(profileId, (err, tombstonedProfile) => {
                t.error(err, 'gets tombstoned profile')

                const state = {
                  ...emptyProfileFields,
                  preferredName: 'Maui',
                  legalName: 'Maui Hangarau',
                  gender: 'other',
                  source: 'webForm',
                  tombstone: {
                    reason: 'user deleted account',
                    date: tombstonedProfile.tombstone.date
                  },
                  authors: {
                    [server.id]: [{ start: 3, end: null }]
                  }
                }
                t.deepEqual(
                  tombstonedProfile,
                  {
                    key: profileId,
                    type: 'person/admin',
                    originalAuthor: server.id,
                    recps: [poBoxId, server.id],
                    ...state,
                    conflictFields: [],
                    states: []
                  },
                  'returns the tombstoned profile'
                )

                server.close(t.end)
              })
            })
          })
        })
      })
    })
  })
})

test('ssb.profile.person.admin.get (will not get public profile)', t => {
  const server = Server()

  // public profile
  const content = {
    type: 'profile/person',
    preferredName: { set: 'Maui Public' },
    tangles: {
      profile: { root: null, previous: null }
    }
    // recps
  }
  server.db.create({ content }, (err, m) => {
    t.error(err, 'creates a public profile')

    const profileId = m.key

    server.profile.person.admin.get(profileId, (err) => {
      t.match(err.message, /not a valid profile\/person\/admin/, 'cant get admin profile using admin person profile api')

      server.close(t.end)
    })
  })
})
