const test = require('tape')
const Server = require('../../test-bot')
const { isMsgId } = require('ssb-ref')
const { promisify: p } = require('util')

const { blob1, blob2, blob3, blob4 } = require('../../lib/blob-helpers.js')

let i = 0
const generateTimestamp = () => {
  return (Date.now() + (i += 10)).toString()
}

const keys = Array.from(Array(10)).map(() => generateTimestamp())

const [
  keyA, keyB, keyC, keyD, keyE, keyF, keyG, keyH, keyI
] = keys

test('profile/person/group custom fields', async t => {
  // ////
  // Setup
  // ////
  const ssb = Server({ tribes: true })

  // create the group and admin subgroup
  const { groupId } = await p(ssb.tribes.create)({})
  // await p(ssb.tribes.subtribe.create)(groupId, { admin: true })

  const customFields = {
    [keyA]: 'Hamilton', // Hometown
    [keyB]: ['Bachelor of Science', 'NCEA'], // Qualifications
    [keyC]: ['Self-employed'], // Employment Status
    [keyD]: true, // Living in NZ
    [keyE]: 26, // age
    [keyF]: 1.23123, // shares
    [keyG]: blob1,
    [keyH]: blob3,
    [keyI]: [blob2, blob4]
  }

  // create an owned profile in the parent group
  let details = {
    preferredName: 'Cherese',
    customFields,
    authors: { add: [ssb.id] },
    recps: [groupId]
  }

  const groupProfileId = await p(ssb.profile.person.group.create)(details)
  t.true(isMsgId(groupProfileId), 'initial person/group profile was made with customFields')

  let profile = await p(ssb.profile.person.group.get)(groupProfileId)
  t.deepEqual(
    profile.customFields,
    customFields,
    'gets the correct initial custom fields'
  )

  // update
  details = {
    customFields: {
      [keyA]: 'Hamilton, New Zealand', // update
      [keyB]: ['Bachelor of Science'], // remove one item
      [keyD]: null, // delete
      [keyG]: blob2,
      [keyI]: [blob1, blob4]
    }
  }

  await p(ssb.profile.person.group.update)(groupProfileId, details)
  profile = await p(ssb.profile.person.group.get)(groupProfileId)

  t.deepEqual(
    profile.customFields,
    {
      [keyA]: 'Hamilton, New Zealand', // Hometown
      [keyB]: ['Bachelor of Science'], // Qualifications
      [keyC]: ['Self-employed'], // Employment Status
      [keyD]: null, // Living in NZ
      [keyE]: 26, // age
      [keyF]: 1.23123, // shares
      [keyG]: blob2,
      [keyH]: blob3,
      [keyI]: [blob1, blob4]
    },
    'gets the correct updated custom fields'
  )

  ssb.close()
  t.end()
})

test('profile/person/admin custom fields', async t => {
  // ////
  // Setup
  // ////
  const ssb = Server({ tribes: true })

  // create the group and admin subgroup
  const { groupId } = await p(ssb.tribes.create)({})
  const { poBoxId } = await p(ssb.tribes.subtribe.create)(groupId, { admin: true, addPOBox: true })

  const customFields = {
    [keyA]: 'Hamilton', // Hometown
    [keyB]: ['Bachelor of Science', 'NCEA'], // Qualifications
    [keyC]: ['Self-employed'], // Employment Status
    [keyD]: true, // Living in NZ
    [keyE]: 26, // age
    [keyF]: 1.23123, // shares
    [keyG]: blob1,
    [keyH]: blob3,
    [keyI]: [blob2, blob4]
  }

  // create an owned profile in the parent group
  let details = {
    preferredName: 'Cherese',
    customFields,
    authors: { add: [ssb.id] },
    recps: [poBoxId]
  }

  const adminProfileId = await p(ssb.profile.person.admin.create)(details)
  t.true(isMsgId(adminProfileId), 'initial person/admin profile was made with customFields')

  let profile = await p(ssb.profile.person.admin.get)(adminProfileId)
  t.deepEqual(
    profile.customFields,
    customFields,
    'gets the correct initial custom fields'
  )

  // update
  details = {
    customFields: {
      [keyA]: 'Hamilton, New Zealand', // update
      [keyB]: ['Bachelor of Science'], // remove one item
      [keyD]: null, // delete
      [keyG]: blob2,
      [keyI]: [blob1, blob4]
    }
  }

  await p(ssb.profile.person.admin.update)(adminProfileId, details)
  profile = await p(ssb.profile.person.admin.get)(adminProfileId)

  t.deepEqual(
    profile.customFields,
    {
      [keyA]: 'Hamilton, New Zealand', // Hometown
      [keyB]: ['Bachelor of Science'], // Qualifications
      [keyC]: ['Self-employed'], // Employment Status
      [keyD]: null, // Living in NZ
      [keyE]: 26, // age
      [keyF]: 1.23123, // shares
      [keyG]: blob2,
      [keyH]: blob3,
      [keyI]: [blob1, blob4]
    },
    'gets the correct updated custom fields'
  )

  ssb.close()
  t.end()
})
