const test = require('tape')
const Server = require('../../test-bot')

// NOTE legacy tests which used to test ssb.profile.create
// have shifted them to test person.group as this is the field with the most fields
// and having coverage of these fields is still good

function runTest (t, update, expectedErr = null, expected) {
  const server = Server({ tribes: true, recpsGuard: true })
  server.tribes.create({}, (err, { groupId, groupInitMsg } = {}) => {
    if (err) throw err

    update.recps = [groupId]
    if (expected) {
      expected.recps = [groupId]
      expected.tangles.group = {
        root: groupInitMsg.key
      }
    }

    if (!update.authors) {
      update.authors = {
        add: [server.id, '*']
      }
    }

    server.profile.person.group.create(update, (err, profileId) => {
      if (expectedErr) {
        if (!err) throw new Error(`expected error matching ${expectedErr}`)
        t.true(err.message.match(expectedErr), `got error matching: ${expectedErr}`)
        // if (!err.message.match(expectedErr)) {
        //   console.error('!!! here is the actual error:')
        //   console.error(err)
        // }

        server.close()
        return
      }

      if (err) throw err

      server.get({ id: profileId, private: true }, (_, value) => {
        // HACK: dont know a good way to test the sequence...?
        expected.authors = value.content.authors
        expected.tangles.group.previous = value.content.tangles.group.previous
        t.deepEqual(value.content, expected, 'output was same as expected')

        server.close()
      })
    })
  })
}

test('ssb.profile.person.group.create', t => {
  t.plan(1)

  const MB = 1024 * 1024
  const A = {
    preferredName: 'Te Ati Awa',
    altNames: {
      add: ['wellington', 'taranaki']
    },
    avatarImage: {
      blob: '&CLbw5B9d5+H59oxDNOy4bOkwIaOhfLfqOLm1MGKyTLI=.sha256',
      unbox: '4bOkwCLbw5B9d5+H59oxDNOyIaOhfLfqOLm1MGKyTLI=.boxs',
      mimeType: 'image/png',
      size: 4 * MB,
      width: 500,
      height: 480
    }
  }
  const expectedA = {
    type: 'profile/person',

    preferredName: { set: 'Te Ati Awa' },
    altNames: {
      wellington: 1,
      taranaki: 1
    },
    authors: {},
    avatarImage: {
      set: {
        blob: '&CLbw5B9d5+H59oxDNOy4bOkwIaOhfLfqOLm1MGKyTLI=.sha256',
        unbox: '4bOkwCLbw5B9d5+H59oxDNOyIaOhfLfqOLm1MGKyTLI=.boxs',
        mimeType: 'image/png',
        size: 4 * MB,
        width: 500,
        height: 480
      }
    },

    tangles: {
      profile: { root: null, previous: null }
    }
  }
  runTest(t, A, null, expectedA)
})

test('ssb.profile.person.group.create - bad attribute (malformed Image)', t => {
  t.plan(1)

  const B = {
    preferredName: 'Te Ati Awa',
    avatarImage: {
      blob: '&asdasd',
      mimeType: 'image/png'
    }
  }
  const expectedB = /avatarImage/
  runTest(t, B, expectedB)
})

test('ssb.profile.person.group.create - bad attribute (malformed altNames)', t => {
  t.plan(1)

  ;(() => {
    const details = {
      preferredName: 'Te Ati Awa',
      altNames: { add: 'dave' }
      // altNames: 'dog'
    }
    const expectedErr = /simpleSet.add/
    runTest(t, details, expectedErr)
  })()
})

test('ssb.profile.person.group.create - bad attribute (malformed authors)', t => {
  t.plan(1)

  ;(() => {
    const details = {
      preferredName: 'Te Ati Awa',
      authors: { add: 'dave' }
    }
    const expectedErr = /authors/
    runTest(t, details, expectedErr)
  })()
})

test('ssb.profile.person.group.create - bad attribute (invalid authors pattern)', t => {
  t.plan(1)

  ;(() => {
    const details = {
      preferredName: 'Te Ati Awa',
      authors: { add: ['@@dave'] }
    }
    const expectedErr = /authors/
    runTest(t, details, expectedErr)
  })()
})

test('ssb.profile.person.group.create - encryption works fine', t => {
  t.plan(1)

  const C = {
    preferredName: 'Te Ati Awa'
  }
  const expectedC = {
    type: 'profile/person',
    preferredName: { set: 'Te Ati Awa' },

    tangles: {
      profile: { root: null, previous: null }
    }
  }
  runTest(t, C, null, expectedC)
})

test('ssb.profile.person.group.create - null types work fine', t => {
  t.plan(1)

  const D = {
    preferredName: null
  }
  const expectedD = {
    type: 'profile/person',
    preferredName: { set: null },

    tangles: {
      profile: { root: null, previous: null }
    }
  }
  runTest(t, D, null, expectedD)
})
