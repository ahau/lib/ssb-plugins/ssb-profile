const test = require('tape')
const CRUT = require('ssb-crut-authors')
const privatePersonSpec = require('../../../spec').person.group

const mockServer = { backlinks: true, query: true }
const { isRoot, isUpdate } = new CRUT(mockServer, privatePersonSpec)

const feedId = '@p76adwR5BjtDkqLTeYyNpV3AY3zmHXCjU+Zht6rsFIA=.ed25519'
const groupId = '%VeNj9x/ZtI87TtSvtOEyw8G3gTrtWDe5OR9tDTAuLrg=.cloaked'

test('spec/person/group isRoot', t => {
  const mock = (override = {}) => {
    const base = {
      type: 'profile/person', // + recps => privatePerson

      preferredName: { set: 'Te Āti Awa' }, // NOTE this text is from an old test, should be a person name!
      legalName: { set: 'Te Āti Awa Ltd.' },
      altNames: { wairarapa: -1, taranaki: 1 },
      authors: { [feedId]: { 1: 1 } },

      description: { set: 'person with roots in tawanaki + wellington' },
      city: { set: 'Palmerston North' },
      country: { set: 'New Zealand' },
      postCode: { set: '123123' },
      // NOTE: no address, email, phone
      profession: { set: 'Web developer' },
      education: {
        set: [
          'Computing Cert'
        ]
      },
      school: {
        set: [
          'Dev Academy'
        ]
      },
      gender: { set: 'female' },
      aliveInterval: { set: '1984-04-04/' },
      deceased: { set: false },
      placeOfBirth: { set: 'hamilton' },
      placeOfDeath: { set: 'tamaki makaurau' },
      buriedLocation: { set: 'N/A' },
      birthOrder: { set: 1 },
      avatarImage: {
        set: {
          blob: '&1ZQM7TjQHBUEcdBijB6y7dkX047wCf4aXcjFplTjrJo=.sha256',
          unbox: 'YmNz1XfPw/xkjoN594ZfE/JUhpYiGyOOQwNDf6DN+54=.boxs',
          mimeType: 'image/png',
          size: 512,
          height: 300,
          width: 300
        }
      },
      headerImage: {
        set: {
          blob: '&TDDasddQHBUEcdBijB6y7dkX047wCf4aXcjFasdaRas=.sha256',
          mimeType: 'image/jpg'
        }
      },

      tangles: {
        profile: { root: null, previous: null }
      },

      recps: [groupId]
    }

    return Object.assign(base, override)
  }

  // all fields
  t.true(isRoot(mock()), 'all fields')
  t.equal(isRoot.errors, null, 'all fields => no errors')

  // minimum fields
  t.true(isRoot({
    type: 'profile/person',
    authors: {
      [feedId]: { 1: 1 }
    },
    tangles: {
      profile: { root: null, previous: null }
    },
    recps: [groupId]
  }), 'minimum fields')

  // tangles
  const update = mock({
    tangles: {
      profile: {
        root: '%og6G1zCagfm7iodppYz10ytwhjubROjzpncU+Tfe6vU=.sha256',
        previous: ['%og6G1zCagfm7iodppYz10ytwhjubROjzpncU+Tfe6vU=.sha256']
      }
    }
  })
  t.false(isRoot(update), 'updates are not roots')

  // avatarImage
  const brokenAvatarImage = mock({
    avatarImage: 'my picture!'
  })
  t.false(isRoot(brokenAvatarImage), 'broken avatarImage not allowed')
  const brokenAvatarImage2 = mock({
    avatarImage: {
      set: {
        blob: '&pooop'
      }
    }
  })
  t.false(isRoot(brokenAvatarImage2), 'broken avatarImage not allowed')

  t.false(
    isRoot(mock({
      type: 'person' // << expect 'profile/..'
    })),
    'type must start with "profile/"'
  )

  t.false(
    isRoot(mock({ legalName: 2 })),
    'legalName must be a string'
  )

  t.false(
    isRoot(mock({
      altNames: { wairarapa: 2.5 }
    })),
    'reject altNames with float weights'
  )

  t.false(
    isRoot(mock({
      altNames: { wairarapa: 'dogo' } // << expect integer values
    })),
    'reject altNames with string weights'
  )

  // TODO change all the failing type tests to this format
  // the other format is only useful debugging cases where spec.isRoot == true
  t.false(isRoot(mock({ description: null })), 'description: null is not valid')
  t.false(isRoot(mock({ description: { set: 2 } })), 'description: {set: 2} is not valid')

  t.false(
    isRoot(mock({ gender: { set: 'dog' } })),
    'gender must be of types male | female | other | unknown'
  )

  t.false(
    isRoot(mock({ aliveInterval: { set: 'dog' } })),
    'aliveInterval must be an edtf time (not random string)'
  )
  t.false(
    isRoot(mock({ aliveInterval: { set: new Date() } })),
    'aliveInterval must be an integer (not Date)'
  )
  t.false(
    isRoot(mock({ aliveInterval: { set: '1901-01-05' } })),
    'aliveInterval must be an interval not just a date'
  )

  t.false(
    isRoot(mock({ birthOrder: { set: 0 } })),
    'birthOrder must be greater than 0'
  )
  t.false(
    isRoot(mock({ birthOrder: { set: 1.5 } })),
    'birthOrder must be an integer'
  )
  t.false(
    isRoot(mock({ birthOrder: { set: 'dog' } })),
    'birthOrder must be an integer (not string)'
  )

  // no junk!
  t.false(
    isRoot(mock({ junk: 'yeah take this!' })),
    'does not except random fields'
  )

  t.end()
})

test('spec/person/group isUpdate', t => {
  const mock = (override = {}) => {
    const base = {
      type: 'profile/person',

      preferredName: { set: 'Te Āti Awa' },
      legalName: { set: 'Te Āti Awa Ltd.' },
      altNames: { wairarapa: -1, taranaki: 1 },

      description: { set: 'person with roots in tawanaki + wellington' },
      city: { set: 'Palmerston North' },
      country: { set: 'New Zealand' },
      postCode: { set: '123123' },
      profession: { set: 'Web developer' },
      education: {
        set: [
          'NCEA Level 1',
          'NCEA Level 2',
          'NCEA Level 3'
        ]
      },
      school: {
        set: [
          'Palmerston North High School'
        ]
      },
      gender: { set: 'female' },
      aliveInterval: { set: '1984-04-04/' },
      deceased: { set: false },
      placeOfBirth: { set: 'hamilton' },
      placeOfDeath: { set: 'tamaki makaurau' },
      buriedLocation: { set: 'N/A' },
      birthOrder: { set: 1 },
      avatarImage: {
        set: {
          blob: '&1ZQM7TjQHBUEcdBijB6y7dkX047wCf4aXcjFplTjrJo=.sha256',
          unbox: 'YmNz1XfPw/xkjoN594ZfE/JUhpYiGyOOQwNDf6DN+54=.boxs',
          mimeType: 'image/png',
          size: 512,
          height: 300,
          width: 300
        }
      },
      headerImage: {
        set: {
          blob: '&TDDasddQHBUEcdBijB6y7dkX047wCf4aXcjFasdaRas=.sha256',
          mimeType: 'image/jpg'
        }
      },

      tombstone: {
        set: {
          date: Date.now(), // => big integer
          reason: 'duplicate'
        }
      },

      tangles: {
        profile: {
          root: '%og6G1zCagfm7iodppYz10ytwhjubROjzpncU+Tfe6vU=.sha256',
          previous: [
            '%47drdibEae85Bo1+qKYKbH2Hlkb+CV6afDioPAe/QiE=.sha256',
            '%rMeiWMvDx0Hgy8d5Zn4QM5ldJNDeIIF3fkoSYlO3iQc=.sha256'
          ]
        }
      },

      recps: [groupId]
    }

    return Object.assign(base, override)
  }

  // all fields
  t.true(isUpdate(mock()), 'all fields')
  t.equal(isUpdate.errors, null, 'all fields => no errors')

  // minimum fields
  t.true(isUpdate({
    type: 'profile/person',
    tangles: {
      profile: {
        root: '%og6G1zCagfm7iodppYz10ytwhjubROjzpncU+Tfe6vU=.sha256',
        previous: [
          '%rMeiWMvDx0Hgy8d5Zn4QM5ldJNDeIIF3fkoSYlO3iQc=.sha256'
        ]
      }
    },
    recps: [groupId]
  }), 'minimum fields')

  // tangles
  const root = mock({
    tangles: {
      profile: { root: null, previous: null }
    }
  })
  t.false(isUpdate(root), 'root msg not an update')

  const lazyPrevious = mock({
    tangles: {
      profile: {
        root: '%og6G1zCagfm7iodppYz10ytwhjubROjzpncU+Tfe6vU=.sha256',
        previous: '%og6G1zCagfm7iodppYz10ytwhjubROjzpncU+Tfe6vU=.sha256'
      }
    }
  })
  t.false(isUpdate(lazyPrevious), 'previous must be an Array!')
  t.notEqual(isUpdate.errors, null, 'errors when previous not an Array')

  const branchFormat = mock({
    root: '%og6G1zCagfm7iodppYz10ytwhjubROjzpncU+Tfe6vU=.sha256',
    branch: ['%og6G1zCagfm7iodppYz10ytwhjubROjzpncU+Tfe6vU=.sha256']
  })
  delete branchFormat.tangles
  t.false(isUpdate(branchFormat), 'old style')

  // other fields
  // all those are same as root tests, so see there

  t.false(
    isUpdate(mock({ junk: 'yeah take this!' })),
    'does not except random fields'
  )

  t.true(
    isUpdate(
      mock({
        headerImage: {
          set: null
        }
      })
    ),
    'can set headerImage to be null'
  )

  t.end()
})
